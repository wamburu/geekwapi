'use strict';

module.exports = {
	app: {
		title: 'GeekWapi',
		description: 'A place for developers to find hackathons, events and meetups',
		keywords: 'Geekwapi, hackathon find, events, meetups',
		googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'geekwapi',
	sessionCollection: 'sessions'
};
